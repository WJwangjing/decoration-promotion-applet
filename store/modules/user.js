export const state = {
    //用户数据
    userInfo: {},
    weChatMobile:''
};
export const mutations = {
    //储存用户信息
    setUserInfo(state, data) {
        if (data) {
            console.log(data)
            state.userInfo = Object.assign({}, state.userInfo, data);
            // #ifdef H5
            window.sessionStorage.setItem('userInfo', JSON.stringify(state.userInfo));
            // #endif
            // #ifndef H5
            uni.setStorageSync('userInfo', state.userInfo);
            // #endif
        }
    },
    // 退出APP
    emptyUserInfo(state) {
        state.userInfo = {};
        // #ifdef H5
        window.sessionStorage.removeItem("userInfo");
        // #endif
        // #ifndef H5
        uni.removeStorageSync("userInfo");
        // #endif
    },
    setWeChatMobile(state, data) {
        // if (data) {
            console.log(data)
            state.weChatMobile = data;
            // #ifdef H5
            window.sessionStorage.setItem('weChatMobile', JSON.stringify(state.weChatMobile));
            // #endif
            // #ifndef H5
            uni.setStorageSync('weChatMobile', state.weChatMobile);
            // #endif
        // }
    },
};
export const actions = {

};