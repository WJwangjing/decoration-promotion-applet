import Vue from 'vue'
import App from './App'
//数据管理中心
import store from '@/store'
Vue.prototype.$store = store;
// 工具
import '@/plugins/utils.js';
//权限配置中心
import base from '@/config/baseUrl'
Vue.prototype.$base = base;
// uview
// import uView from "@/uview/uview-ui";
// Vue.use(uView);
import tTable from '@/components/t-table/t-table.vue';
import tTh from '@/components/t-table/t-th.vue';
import tTr from '@/components/t-table/t-tr.vue';
import tTd from '@/components/t-table/t-td.vue';
Vue.component('tTable', tTable);
Vue.component('tTh', tTh);
Vue.component('tTr', tTr);
Vue.component('tTd', tTd);
import MoreTree from '@/components/xiaolu-tree/tree.vue';
Vue.component('MoreTree', MoreTree);

//点击放大图片
const showBigImg = (imgsArray) => {
    let arr = [];
    arr.push(imgsArray)
    console.log(arr, '图片的地址')
    uni.previewImage({
        current: 0,
        urls: arr //必须是网址路径，否则加载不出来，如：http：或https：
    });
}
Vue.prototype.$showBigImg = showBigImg;


uni.$u.config.unit = 'rpx'
//挂载全局http请求
import $http from '@/config/requestConfig'
Vue.prototype.$http = $http;
// #ifdef MP-WEIXIN
//挂载全局微信分享
import {
    wxShare
} from '@/config/utils'
Vue.prototype.wxShare = wxShare;
// #endif
//判断是否登录
import {
    judgeLogin
} from '@/config/login';
Vue.prototype.judgeLogin = judgeLogin;
Vue.config.productionTip = false;
// #ifdef H5
//微信SDK
import '@/plugins/wxJsSDK.js';
// #endif
//全局组件
import MescrollBody from "@/components/mescroll-uni/mescroll-body.vue";
Vue.component("mescroll-body", MescrollBody);
App.mpType = 'app'

import uView from '@/uni_modules/uview-ui'
Vue.use(uView)
const app = new Vue({
    store,
    ...App
})
app.$mount();